/*
*   Copyright (c) 2020 Johannes Thorén
*   All rights reserved.

*   Permission is hereby granted, free of charge, to any person obtaining a copy
*   of this software and associated documentation files (the "Software"), to deal
*   in the Software without restriction, including without limitation the rights
*   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*   copies of the Software, and to permit persons to whom the Software is
*   furnished to do so, subject to the following conditions:

*   The above copyright notice and this permission notice shall be included in all
*   copies or substantial portions of the Software.

*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*   SOFTWARE.
*/
extern crate serde_json;

use std::fs::File;
use std::io::prelude::*;

pub fn read_file(file_path: &str) -> String {
    let mut file = File::open(file_path).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    return contents;
}

pub fn parse_data(data: &str) -> serde_json::Value {
    let data = serde_json::from_str(data);
    let parsed: serde_json::Value = data.unwrap();

    return parsed;
}

pub fn move_task(
    index: usize,
    source_group: &str,
    reciving_group: &str,
    json_data: &mut serde_json::Value,
) {
    let mut source_group = json_data["groups"][source_group].as_array_mut().unwrap();
    let temp_data = source_group[index].clone();
    source_group.remove(index);

    let mut receiving_group = json_data["groups"][reciving_group].as_array_mut().unwrap();
    receiving_group.push(temp_data);

    let data = format!("{}", json_data);
    write_to_file(
        data.as_str(),
        format!("{}.json", json_data["project"].as_str().unwrap()).as_str(),
    )
}

pub fn add_task(group: &str, string: &str, json_data: &mut serde_json::Value) {
    let mut group = json_data["groups"][group].as_array_mut().unwrap();
    group.push(serde_json::Value::String(String::from(string)));

    let data = format!("{}", json_data);
    write_to_file(
        data.as_str(),
        format!("{}.json", json_data["project"].as_str().unwrap()).as_str(),
    )
}

pub fn remove_task(group: &str, index: usize, json_data: &mut serde_json::Value) {
    let mut group = json_data["groups"][group].as_array_mut().unwrap();
    group.remove(index);

    let data = format!("{}", json_data);
    write_to_file(
        data.as_str(),
        format!("{}.json", json_data["project"].as_str().unwrap()).as_str(),
    )
}

pub fn add_group(group_name: &str, json_data: &serde_json::Value) {
    let mut temp = String::from("");
    for n in json_data["groups"].as_object().unwrap() {
        let f: String = format!("\"{}\":{},", n.0.as_str(), n.1);
        temp.push_str(f.as_str());
    }

    let project_name = json_data["project"].as_str().unwrap();
    let new_group = format!("\"{}\":[]", group_name);

    let value = format!(
        "{{\"project\":\"{}\",\"groups\":{{{}{}}}}}",
        project_name, temp, new_group
    );

    write_to_file(value.as_str(), format!("{}.json", project_name).as_str());
}

pub fn remove_group(group_name: &str, json_data: &serde_json::Value) {
    let data = json_data["groups"].as_object().unwrap();
    let mut temp = String::from("");
    for n in data.iter().enumerate() {
        if (n.1).0 != group_name {
            let f: String = format!("\"{}\":{},", (n.1).0, (n.1).1);
            temp.push_str(f.as_str());
        }
        if n.0 == data.len() - 1 {
            //println!("n : [{}], dl : [{}]", n.0, (data.len() - 1));
            match temp.pop() {
                Some(_) => {}
                None => {}
            }
        }
    }

    let project_name = json_data["project"].as_str().unwrap();

    let value = format!(
        "{{\"project\":\"{}\",\"groups\":{{{}}}}}",
        project_name, temp,
    );

    write_to_file(value.as_str(), format!("{}.json", project_name).as_str())
}

fn write_to_file(new_data: &str, file: &str) {
    let mut f = File::create(file).unwrap();
    f.write_all(new_data.as_bytes()).unwrap();
}

pub fn new_project(name: &str) {
    let mut file = File::create(format!("{}.json", name).as_str()).unwrap();
    file.write_all(format!("{{ \"project\": \"{}\",\"groups\":{{}}}}", name).as_bytes())
        .unwrap();
}

pub fn get_project_name(data: &serde_json::Value) -> String {
    let project_name = data["project"].as_str().unwrap();
    return String::from(project_name);
}

pub fn print_groups (json_data: &serde_json::Value) {
    let data = json_data["groups"].as_object().unwrap();

    println!("\n== all groups ==\n");
    for n in data.iter() {
        println!(" * {}", n.0);
        println!("-----------------------");
    }
}