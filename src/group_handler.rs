/*
 *   Copyright (c) 2020 Johannes Thorén
 *   All rights reserved.

 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
extern crate serde_json;
pub struct SelectedGroup {
    pub group_name: String,
    pub group_data: Vec<serde_json::Value>
}


pub fn group_select( group_name: &str,  _json_data: &serde_json::Value) -> SelectedGroup{
    let group_data = _json_data["groups"][group_name].as_array().unwrap();
    
    SelectedGroup {
    group_name: String::from( group_name),
    group_data: group_data.to_vec(),
    }
}

pub fn showgroup(data: &SelectedGroup) {

    println!("====== Group : {} ======\n", &data.group_name);

    let mut index = 0;
    for n in &data.group_data {
        println!("{} {}\n-----------------------",index, n.as_str().unwrap());
        index += 1;
    }
}