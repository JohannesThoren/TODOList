/*
*   Copyright (c) 2020 Johannes Thorén
*   All rights reserved.

*   Permission is hereby granted, free of charge, to any person obtaining a copy
*   of this software and associated documentation files (the "Software"), to deal
*   in the Software without restriction, including without limitation the rights
*   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*   copies of the Software, and to permit persons to whom the Software is
*   furnished to do so, subject to the following conditions:

*   The above copyright notice and this permission notice shall be included in all
*   copies or substantial portions of the Software.

*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*   SOFTWARE.
*/

enum States {
    MainMenu,
    TodoListMenu,
    GroupMenu,
    CreateNew,
}

mod data_handler;
mod group_handler;

use clap::*;
use std::io;
fn main() {
    let mut data = String::new();
    let mut parsed_data: serde_json::Value = serde_json::Value::Null;
    let mut path = String::new();
    let mut selected = Option::<group_handler::SelectedGroup>::None;

    let mut input = String::new();

    let mut state = States::MainMenu;

    let mut running = true;
    while running {
        input = String::from("");
        print!("\x1B[2J\x1B[1;1H");
        match state {
            States::MainMenu => {
                println!("===== Main Menu =====");
                println!("[o]pen a list");
                println!("[n]ew todo list");
                println!("[e]xit the program");
                match io::stdin().read_line(&mut input) {
                    Ok(_) => match &input.trim_end() as &str {
                        "o" | "open" => {
                            println!("enter filepath : ");
                            input = String::from("");
                            io::stdin().read_line(&mut path);
                            data = data_handler::read_file(&path.trim_end());
                            parsed_data = data_handler::parse_data(data.as_str());
                            state = States::TodoListMenu;
                        }
                        "n" | "new" => {
                            let mut tmp = String::new();
                            println!("project name (list name) : ");
                            io::stdin().read_line(&mut tmp);
                            data_handler::new_project(&tmp.trim_end());
                        }
                        "e" | "exit" => {
                            running = false;
                        }
                        _ => {}
                    },
                    Err(e) => {}
                }
            }
            States::CreateNew => {}
            States::TodoListMenu => {


                println!(
                    "===== List : {} =====",
                    data_handler::get_project_name(&parsed_data)
                );

                data_handler::print_groups(&parsed_data);

                println!("[c]hoose group");
                println!("[a]dd group");
                println!("[r]emove group");
                println!("[e]xit");
                match io::stdin().read_line(&mut input) {
                    Ok(_) => match &input.trim_end() as &str {
                        "c" | "choose" => {
                            println!("enter group name : ");
                            let mut tmp = String::new();
                            io::stdin().read_line(&mut tmp);
                            selected =
                                Some(group_handler::group_select(tmp.trim_end(), &parsed_data));
                            state = States::GroupMenu;
                        }
                        "a" | "add" => {
                            println!("enter name of the new group : ");
                            let mut tmp = String::new();
                            io::stdin().read_line(&mut tmp);
                            data_handler::add_group(tmp.trim_end(), &parsed_data);
                        }
                        "r" | "remove" => {
                            println!("enter name of the new group : ");
                            let mut tmp = String::new();
                            io::stdin().read_line(&mut tmp);
                            data_handler::remove_group(tmp.trim_end(), &parsed_data);
                        }
                        "e" | "exit" => {
                            state = States::MainMenu;
                        }

                        _ => {}
                    },
                    Err(e) => {}
                }
                data = data_handler::read_file(&path.trim_end());
                parsed_data = data_handler::parse_data(data.as_str());
            }
            States::GroupMenu => {

                match &selected {
                    Some(res) => {
                        group_handler::showgroup(&res);
                    }
                    None => {}
                }
                println!("[b]ack to menu");
                println!("[m]ove task to another group");
                println!("[a]dd task");
                println!("[r]emove task");

                let current_group = match &selected {
                    Some(res) => {
                        res.group_name.clone()
                    }
                    None => { 
                        String::from("null")
                    }
                };
                match io::stdin().read_line(&mut input) {
                    Ok(_) => match &input.trim_end() as &str {
                        "b" | "back" => {
                            state = States::TodoListMenu;
                        }
                        "m" | "move" => {
                            println!("index of task to move : ");
                            let mut tmp = String::new();
                            io::stdin().read_line(&mut tmp);
                            let index = tmp.trim_end().parse::<usize>().unwrap();
                            println!("receiving list : ");
                            let mut rcv = String::new(); 
                            io::stdin().read_line(&mut rcv);
                            
                            data_handler::move_task(index, &current_group, rcv.trim_end(), &mut parsed_data,
                            );
                        }
                        "a" | "add" => {
                            let mut task = String::new(); 
                            println!("task : ");
                            io::stdin().read_line(&mut task);
                            data_handler::add_task(current_group.as_str(), task.trim_end(), &mut parsed_data);
                        }
                        "r" | "remove" => {
                            println!("index of task to remove : ");
                            let mut tmp = String::new();
                            io::stdin().read_line(&mut tmp);
                            let index = tmp.trim_end().parse::<usize>().unwrap();
                            data_handler::remove_task(&current_group, index, &mut parsed_data);

                        }
                        _ => {}
                    },
                    Err(e) => {}
                }
                data = data_handler::read_file(&path.trim_end());
                parsed_data = data_handler::parse_data(data.as_str());
                selected = Some(group_handler::group_select(current_group.as_str(), &parsed_data));
            }
        }
    }
}
